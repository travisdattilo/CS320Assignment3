Travis Dattilo  
travisdattilo@yahoo.com  

"prog3_1.cpp"  
This is a simple C++ program that takes the first command line argument, being a lua file, from stdin,
and creates a lua environment to execute that file.

"prog3_2.lua"  
This lua file contains the InfixToPostfix function that takes a string argument that will convert an infix
algebra statement to a postfix representation. Within this function I have an operation stack (a lua table)
that is a container for pushing and popping operators on and off as needed. I also have helper functions
inside the InfixToPostfix function that are used when actually examining the input string. The process for
converting the input string from infix to postfix is as follows: tokenize the input on white space between
each character, looping through each token check whether it is an operation symbol or not, and based on this
distinction, operators will be pushed and popped off the operater stack (a lua table) named "opStack" and operands
will be pushed onto an output table named "postFix." After all the insertions and removals from both of these
containers occurs, both tables are concatenated together and returned as one single string.

"prog3_3.cpp"  
This is a C++ program that uses the same technique from "prog3_1.cpp" of taking the first command line argument,
being a lua file, from stdin, and creates a lua environment that executes that file; specifically this program is
meant to use the InfixToPostfix function. This program takes a string input from stdin from the user that will be 
the string to send to the lua function written in "prog3_2.lua." The return value from the InfixToPostfix function
is then sent back to this C++ program and printed on stdout. 

"cs320programmingrubric.pdf"  
This rubric is required for grading purposes.


