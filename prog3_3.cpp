extern "C" {
    #include "lua.h"
    #include "lauxlib.h"
    #include "lualib.h"
}

#include <iostream>
#include <string>

using namespace std; 

int main(int argc, char* argv[]){

    cout << "Assignment #3-3, Travis Dattilo, travisdattilo@yahoo.com" << endl;    

    lua_State *L = luaL_newstate();
    luaL_openlibs(L);
    luaL_dofile(L,argv[1]);   
        
    string input;
    getline(cin, input);

    lua_getglobal(L, "InfixToPostfix");
    lua_pushstring(L,input.c_str());

    lua_pcall(L,1,1,0);

    const char * output = lua_tostring(L, -1);

    cout << output << endl;
    
    lua_close(L);
    return 0;
}
