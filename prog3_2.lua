function InfixToPostfix(str)
    print("Assignment #3-2, Travis Dattilo, travisdattilo@yahoo.com")
    input = str
    opStack = {}
    postFix = {}
    
    function contains(table, item)
        for k,v in ipairs(table) do
            if v == item then
                return true
            end
        end
        return false
    end

    function isEmpty(table)
        if next(table) == nil then
            return true
        end
        return false
    end

    function checkPrecedence(operatorFromStack,incomingOperator)
        if operatorFromStack == "*" or operatorFromStack == "/" then
            operatorFromStackLevel = 3
        elseif operatorFromStack == "+" or operatorFromStack == "-" then
            operatorFromStackLevel = 2
        end
        
        if incomingOperator == "*" or incomingOperator == "/" then
            incomingOperatorLevel = 3
        elseif incomingOperator == "+" or incomingOperator == "-" then
            incomingOperatorLevel = 2
        end
        
        if operatorFromStackLevel == incomingOperatorLevel then
            return "equal"
        elseif operatorFromStackLevel > incomingOperatorLevel then
            return "greater"
        elseif operatorFromStackLevel < incomingOperatorLevel then
            return "lesser"
        end
    end

    for i in input.gmatch(input, "%S+") do
        if i ~= "*" and i ~= "/" and i ~= "+" and i ~= "-" then
            table.insert(postFix,i)
        elseif isEmpty(opStack) then
            table.insert(opStack,i)
        elseif checkPrecedence(opStack[#opStack],i) == "lesser" then
            table.insert(opStack,i)
        elseif checkPrecedence(opStack[#opStack],i) == "equal" then
            table.insert(postFix,opStack[#opStack])
            table.remove(opStack)
            table.insert(opStack,i)
        elseif checkPrecedence(opStack[#opStack],i) == "greater" then
            while(checkPrecedence(opStack[#opStack],i) == "greater") do
                table.insert(postFix,opStack[#opStack])
                table.remove(opStack)
            end
            table.insert(postFix,opStack[#opStack])
            table.remove(opStack)
            table.insert(opStack,i)
        end
    end

    a = table.concat(postFix, " ")
    b = table.concat(opStack," ")
    c = a.." "..b
    return c
end
