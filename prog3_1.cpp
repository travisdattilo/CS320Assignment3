//Single command line arguement which is name of a lua file
//Program executes lua file in lua enviroment
//Use lua-5.3.4 source

extern "C" {
    #include "lua.h"
    #include "lauxlib.h"
    #include "lualib.h"
}

#include <iostream>

using namespace std; 

int main(int argc, char* argv[]){

    //create a new lua state
    lua_State *L = luaL_newstate();
    
    //open all libraries
    luaL_openlibs(L);
    
    cout << "Assignment #3-1, Travis Dattilo, travisdattilo@yahoo.com" << endl;
    luaL_dofile(L,argv[1]);   

    lua_close(L);
    return 0;

}
